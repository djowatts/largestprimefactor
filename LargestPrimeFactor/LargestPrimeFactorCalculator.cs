﻿using System;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace LargestPrimeFactor
{
//    Largest prime factor
//Problem 3
//The prime factors of 13195 are 5, 7, 13 and 29.

//What is the largest prime factor of the number 600851475143 ?
    /// <summary>
    /// Largest prime factor
    /// Problem 3
    /// The prime factors of 13195 are 5, 7, 13 and 29.
    /// What is the largest prime factor of the number 600851475143?
    /// </summary>
    public static class LargestPrimeFactorCalculator
    {
        public static long Calculate(long input)
        {
           return Enumerable.Range(2, Convert.ToInt32(Math.Sqrt(input)))
                .Reverse()
                .AsParallel().FirstOrDefault(g => input % g == 0 && IsPrime(g));
        }

        private static bool IsPrime(long number)
        {
            var ceiling = Math.Floor(Math.Sqrt(number));

            if (number == 1) return false;

            if (number != 2)
            {
                for (var i = 2; i <= ceiling; i++)
                {
                    if (number%i == 0) return false;
                }
            }

            return true;
        }
    }

    public class Tests
    {
        [Test]
        public void PrimeFactorsOf13195_CalcualtedCorrectly()
        {
            var result = LargestPrimeFactorCalculator.Calculate(13195);
            result.Should().Be(29);
        }

        [Test]
        public void PrimeFactorsOf100_CalculatedCorrectly()
        {
            var result = LargestPrimeFactorCalculator.Calculate(100);
            result.Should().Be(5);
            
        }


        /// <summary>
        /// The aim of this Kata was to find the largest prime factor of 600851475143.
        /// That value should be 6857
        /// </summary>
        [Test]
        public void PrimeFactorsOf_600851475143()
        {
            var result = LargestPrimeFactorCalculator.Calculate(600851475143);
            result.Should().Be(6857);
        }
    }
}
